module.exports = function(grunt) {
    grunt.initConfig({
        clean: ['build', 'resume.dev.html'],
        markdown: {
            options: {
                template: 'templates/template.jst',
                preCompile: function(src, context) {
                    context.head = grunt.file.read(context.head);
                    context.script = grunt.file.read(context.script);
                }
            },
            dev: {
                files: {
                    'resume.dev.html': 'formats/resume.md'
                },
                options: {
                    templateContext: {
                        head: 'templates/head.dev.html',
                        script: 'templates/script.dev.html'
                    }
                }
            },
            inline: {
                files: {
                    'formats/resume.html': 'formats/resume.md'
                },
                options: {
                    templateContext: {
                        head: 'templates/head.html',
                        script: 'templates/script.html'
                    }
                }
            }
        },
        cssUrlPrefix: {
            inline: {
                options: {
                    prefix: 'https://gitlab.com/nttibbetts/resume/raw/master'
                },
                files: {
                    'build/styles.css': 'styles.css'
                }
            }
        },
        less: {
            bootstrap: {
                options: {
                    paths: 'bower_components/bootstrap/less/'
                },
                files: {
                    'build/bootstrap.css': 'less/bootstrap.less'
                }
            }
        },
        cssmin: {
            bootstrap: {
                files: {
                    'build/bootstrap.min.css': 'build/bootstrap.css'
                }
            },
            inline: {
                files: {
                    'build/styles.min.css': 'build/styles.css'
                }
            }
        },
        uglify: {
          inline: {
            files: {
              'build/polyfill.min.js': 'polyfill.js',
              'build/styles.min.js': 'styles.js'
            }
          }
        },
        htmlbuild: {
            inline: {
                options: {
                    scripts: {
                        polyfill: 'build/polyfill.min.js',
                        styles: 'build/styles.min.js'
                    },
                    styles: {
                        bootstrap: 'build/bootstrap.min.css',
                        styles: 'build/styles.min.css'
                    }
                },
                files: {
                    'formats/resume.html': 'formats/resume.html'
                }
            }
        },
        generatePdf: {
            html: {
                files: {
                    'formats/resume.pdf': 'formats/resume.html'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-markdown');
    grunt.loadNpmTasks('grunt-html-build');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadTasks('tasks');

    grunt.registerTask('bootstrap', ['less:bootstrap', 'cssmin:bootstrap']);

    grunt.registerTask('dev', ['clean', 'less:bootstrap', 'markdown:dev']);
    grunt.registerTask('inline', ['bootstrap', 'markdown:inline', 'cssUrlPrefix:inline', 'cssmin:inline', 'uglify:inline', 'htmlbuild:inline']);
    grunt.registerTask('pdf', ['generatePdf:html']);

    grunt.registerTask('default', ['clean', 'inline', 'pdf']);
};
