# Nathan Tibbetts
<nttibbetts@gmail.com>  
508-769-0201  
Greater Boston Area

_The code that generated this file can be found at my [resume repo][repo]_

## Skills

### Proficiency
 * Javascript, HTML, CSS
 * Grunt, Bower, Require.js, Marionette, Bootstrap 3, Scss
 * Git, Jira, Confluence, ESLint, JSLint
 * Vim
 * Chrome, Firefox, Edge
 * Linux (Gentoo, Debian, Ubuntu)

### Prior Experience
 * Java, SQL, XML, Python, Perl, Bash
 * Webpack, Babel, AngularJS, Bootstrap 4, Foundation, Less
 * Subversion, CVS
 * Eclipse, Emacs
 * LibreOffice, IE8+
 * Windows (8, 7, XP)

## Experience

### [CloudHealth Technologies][cloudhealth], Boston, MA
#### Software Engineer - Aug 2016 to Present

##### [Core SaaS Product][cloudhealth-app]
 * Involved in implementing two UX design rebrandings
 * Added marketing area on login page
 * Improved usuability and performance of perspective editor
 * Helped extend and maintain existing functionality
 * Mentored other software engineers in front end technologies
 * Made code quality and consistency improvements
 * Increased code coverage by adding more tests

### [Wanderu][wanderu], Boston, MA
#### Software Engineer - Jun 2015 to Jul 2016

##### [Search Web App][wanderu]
 * Helped finish and launch new search SPA built on Backbone
 * Built and launched new version of search widget
 * Updated search web app for launch of Mexico [site][wanderu-mx]
 * Implemented changes to improve page ranking
 * Made numerous fixes and UI/UX improvements
 * Mentored junior front end software engineer

### [Embed.ly][embedly], Boston, MA
#### Software Engineer - Feb 2014 to Sep 2014

##### [Analytics Dashboard][embedly-app]
 * Upgraded Ember and supporting libraries to latest version and made any changes necessary from upgrade
 * Brought Ember code closer to current Ember best practices
 * Worked with UI Designer to implement new look and feel for the application
 * Implemented new REST APIs or changed existing ones in the backend as needed
 * Added new graph visualizations for customer data using NVD3

## Education

### [Worcester Polytechnic Institute][wpi], Worcester, MA
#### Bachelor of Science in Computer Science - May 2007
Related Course Work:

 * Software Engineering, OO Analysis and Design, Algorithms
 * Computer Networks, Database Systems, Distributed Computer Systems


[repo]: http://gitlab.com/nttibbetts/resume
[cloudhealth]: https://www.cloudhealthtech.com
[cloudhealth-app]: https://apps.cloudhealthtech.com
[wanderu]: https://www.wanderu.com
[wanderu-mx]: https://www.wanderu.mx
[embedly]: http://www.embed.ly
[embedly-app]: http://app.embed.ly
[wpi]: http://www.wpi.edu
