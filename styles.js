(function() {
    function wrap_list(header_id) {
        var header = document.getElementById(header_id),
            list = header.nextElementSibling,
            insert = list.nextElementSibling,
            wrapper = document.createElement('div');

        wrapper.className = 'col-xs-6';
        header.parentElement.insertBefore(wrapper, insert);
        wrapper.appendChild(header);
        wrapper.appendChild(list);
        return wrapper
    }

    var proficiency = wrap_list('proficiency');
    var prior_experience = wrap_list('prior-experience');
    var container = document.querySelector('.container');
    var experience = document.getElementById('experience');
    var row = document.createElement('div');

    row.className = 'row';
    container.insertBefore(row, experience);
    row.appendChild(proficiency)
    row.appendChild(prior_experience)
})();
