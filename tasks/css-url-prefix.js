module.exports = function(grunt) {
    var urlRegex = /url\(([\'\"])?(.*)([\'\"])?\)/g;

    grunt.registerMultiTask('cssUrlPrefix', function() {
        var options = this.options({
            separator: grunt.util.linefeed
        });

        if (!options.prefix) {
            grunt.fail.warn('Required options property "prefix" missing.');
        }

        options.prefix += options.prefix.substr(-1) === '/' ? '' : '/'

        this.files.forEach(function(file) {
            var src = file.src.filter(function(filepath) {
                if (!grunt.file.exists(filepath)) {
                    grunt.log.warn('Source file"' + filepath + '" not found.');
                    return false;
                } else {
                    return true;
                }
            }).map(function(filepath) {
                var src = grunt.file.read(filepath);

                src = src.replace(urlRegex, function(match, beginQuote, url, endQuote) {
                    beginQuote = beginQuote || '';
                    endQuote = endQuote || '';
                    return 'url(' + beginQuote + options.prefix + url + endQuote + ')';
                });

                return src;
            }).join(options.separator);

            grunt.file.write(file.dest, src);

            grunt.log.writeln('File "' + file.dest + '" created.');
        });
    });
};
