module.exports = grunt => {
    const path = require('path');
    const puppeteer = require('puppeteer');

    grunt.registerMultiTask('generatePdf', function() {
        const done = this.async();

        Promise.all(this.files.map(file => {
            return new Promise(async (resolve, reject) => {
                // this only really makes sense with a single source file
                const src = file.src[0];

                if (!src) {
                    grunt.log.warn('No valid source files provided');
                    reject();
                } else {
                    const browser = await puppeteer.launch();
                    const page = await browser.newPage();
                    const url = 'file://' + path.resolve(src);

                    await page.goto(url, { waitUntil: 'networkidle0' });
                    await page.pdf({
                        path: file.dest,
                        scale: 0.9,
                        width: '10in',
                        height: '13in',
                        margin: {
                            top: '1cm',
                            right: '1cm',
                            bottom: '1cm',
                            left: '1cm'
                        }
                    });

                    await browser.close();
                    resolve();
                }
            });
        })).then(() => done()).catch(() => done());
    });
};
