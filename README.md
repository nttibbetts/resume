Resume
======

My resume in markdown format plus a bit of code to generate some prettier formats.

Formats
-------
Shareable links to all formats including pre built ones of the generated formats:
 * [markdown][fmt-md]
 * [html][fmt-html]
 * [pdf][fmt-pdf]

Development
-----------

[node][node] needs to be [installed][node-pm] first.  
(Note: `v7` or greater of node is needed for `async`/`await` support)  
then clone this repo:
```
git clone https://gitlab.com/nttibbetts/resume.git
```
install grunt-cli and bower:
```
npm install -g grunt-cli
npm install -g bower
```
use npm and bower to install the dependencies
```
npm install
bower install
```
finally, build all the pretty formats:
```
grunt
```

License
-------

![CC-NC-ND][cc-img]  
The files under the formats/ directory are [CC-NC-ND][cc] licensed, it is _my_ resume after all.

![GPLv3][gpl-img]  
The rest of the code is licensed under [GPLv3][gpl].


[fmt-md]: https://gl.githack.com/nttibbetts/resume/raw/master/formats/resume.md
[fmt-html]: https://gl.githack.com/nttibbetts/resume/raw/master/formats/resume.html
[fmt-pdf]: https://gl.githack.com/nttibbetts/resume/raw/master/formats/resume.pdf
[node]: https://nodejs.org/
[node-pm]: https://nodejs.org/en/download/package-manager/
[cc]: http://creativecommons.org/licenses/by-nc-nd/4.0/
[cc-img]: http://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png "CC-NC-ND"
[gpl]: http://www.gnu.org/licenses/gpl-3.0.html
[gpl-img]: http://www.gnu.org/graphics/gplv3-88x31.png "GPLv3"
